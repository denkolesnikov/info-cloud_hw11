<?php
/**
 * Created by PhpStorm.
 * User: Колесников Денис
 * Date: 07.12.2017
 * Time: 21:43
 */

interface Postable
{
  public function getShortPreview();
}