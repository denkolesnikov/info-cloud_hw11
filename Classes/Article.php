<?php
/**
 * Created by PhpStorm.
 * User: Колесников Денис
 * Date: 07.12.2017
 * Time: 21:23
 */

class Article extends Publication
{
    public $author;


    public function __construct($id, $title, $previewText, $text, $author)
    {
        parent::__construct($id ,$title, $previewText, $text);
        $this->author = $author;
    }

    public function getShortPreview(){
        $res = parent::getShortPreview();
        $res .= '<p class="badge source">'.$this->author.'</p>';
        $res .= '<a class="readMore" href="showPublication.php?id='.$this->id.'"><span>read more ></span></a>';
        return $res;
    }

}