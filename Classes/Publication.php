<?php
/**
 * Created by PhpStorm.
 * User: Колесников Денис
 * Date: 07.12.2017
 * Time: 21:21
 */

abstract class Publication
{
    public $id;
    public $title;
    public $previewText;
    public $text;


    public function __construct($id, $title, $previewText, $text)
    {
        $this->id = $id;
        $this->title = $title;
        $this->previewText = $previewText;
        $this->text = $text;
    }

    public static function create($id, PDO $pdo){
        $sql = "SELECT * FROM publications WHERE id = :id";
        try{
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':id',$id);
            $stmt->execute();
            $testRaw = $stmt->fetchObject();
            if(is_null($testRaw->author)){
                $result = new News($testRaw->id, $testRaw->title, $testRaw->preview_text, $testRaw->text ,$testRaw->source);
            }else {
                $result = new Article($testRaw->id, $testRaw->title, $testRaw->preview_text, $testRaw->text, $testRaw->author);
            }
        }catch(PDOException $e){
            $result = 'failed to SELECT database entry<br>'.$e->getMessage();
        }
        return $result;
    }

    public function getShortPreview(){
        $res = '<h3>'.$this->title.'</h3>';
        $res .= '<p>'.$this->previewText.'</p>';
        return $res;
    }

}