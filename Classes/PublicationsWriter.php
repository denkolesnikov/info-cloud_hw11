<?php
/**
 * Created by PhpStorm.
 * User: Колесников Денис
 * Date: 07.12.2017
 * Time: 21:30
 */

class PublicationsWriter implements Postable
{
    public $publications;


    public function __construct($publicationType ,PDO $pdo)
    {
        if($publicationType == 'articles'){
            $sql = 'SELECT * FROM publications WHERE source IS NULL';
            try{
                $res = $pdo->query($sql);
                $pubRaw = $res->fetchAll();
                foreach ($pubRaw as $item){
                    $publication[] = new Article($item['id'], $item['title'], $item['preview_text'],$item['text'], $item['author']);
                }

            }catch(PDOException $e){
                $publication = 'failed to SELECT database entry:'.$publicationType.'<br>'.$e->getMessage();
            }
        }elseif($publicationType == 'news'){
            $sql = 'SELECT * FROM publications WHERE author IS NULL';
            try{
                $res = $pdo->query($sql);
                $pubRaw = $res->fetchAll();
                foreach ($pubRaw as $item){
                    $publication[] = new News($item['id'], $item['title'], $item['preview_text'],$item['text'], $item['source']);
                }
            }catch(PDOException $e){
                $publication = 'failed to SELECT database entry'.$publicationType.'<br>'.$e->getMessage();
            }
        }
        $this->publications = $publication;
    }

    public function getShortPreview(){

    }

}