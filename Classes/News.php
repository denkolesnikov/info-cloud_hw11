<?php
/**
 * Created by PhpStorm.
 * User: Колесников Денис
 * Date: 07.12.2017
 * Time: 21:22
 */

class News extends Publication
{
    public $source;


    public function __construct($id, $title, $previewText, $text, $source)
    {
        parent::__construct($id, $title, $previewText, $text);
        $this->source = $source;
    }

    public function getShortPreview(){
        $res = parent::getShortPreview();
        $res .= '<p class="badge source">'.$this->source.'</p>';
        $res .= '<a class="readMore" href="showPublication.php?id='.$this->id.'"><span>read more ></span></a>';
        return $res;
    }

}