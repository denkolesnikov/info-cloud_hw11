<?php

require_once 'DB/connect_db.php';
require_once 'Interfaces/Postable.php';
require_once 'Classes/Publication.php';
require_once 'Classes/Article.php';
require_once 'Classes/News.php';
require_once 'Classes/PublicationsWriter.php';


/*
require_once 'DB/create_table.php';
require_once 'DB/add_test_data.php';
*/

$articlesObj = new PublicationsWriter('articles',$pdoDB);
$newsObj = new PublicationsWriter('news',$pdoDB);

$articles = $articlesObj->publications;
$news = $newsObj->publications;


?>

<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="styles.css">
        <title>Cloud Info</title>
    </head>
    <body>

        <div class="container leftBlock">
            <h3 class="heading"><u><i>ARTICLES</i></u></h3>
          <?php foreach($articles as $article):?>
              <ul>
                  <li>
                      <?=$article->getShortPreview();?>
                  </li>
              </ul>
          <?php endforeach;?>
        </div>
        <div class="container rightBlock">
            <h3 class="heading"><u><i> NEWS </i></u></h3>
            <?php foreach($news as $item):?>
                <ul>
                    <li>
                        <?=$item->getShortPreview();?>
                    </li>
                </ul>
            <?php endforeach;?>
        </div>

    </body>
</html>