<?php

require_once 'DB/connect_db.php';
require_once 'Classes/Publication.php';
require_once 'Classes/Article.php';
require_once 'Classes/News.php';

if(isset($_GET['id'])){
    $id = $_GET['id'];
    $publication = Publication::create($id,$pdoDB);

    if(property_exists($publication, 'author')){
        $pubBadge = $publication->author;
    }else{
        $pubBadge = $publication->source;
    }

}else exit('id is missing!');

?>

<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="styles.css">
        <title><?=$publication->title?></title>
    </head>
    <body>
        <a href="/" class="back_link"><span><< back</span></a>
        <div class="content">
            <h3 class="text-align"><?=$publication->title?></h3>
            <p><?=$publication->text?></p>
            <hr class="content">
            <p class="badge text-align"><?=$pubBadge?></p>
        </div>
    </body>
</html>

