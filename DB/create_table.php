<?php
require_once 'connect_db.php';

try{
	$sqlQuery = '
		CREATE TABLE publications(
		    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		    title VARCHAR(80),
		    preview_text TEXT,
		    text  TEXT,
		    author VARCHAR(70),
		    source VARCHAR(70)
		)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB 
	';
	$pdoDB->exec($sqlQuery);

}catch(PDOException $e){
	exit('faild to creat table<br>'.$e->getMessage());
}
