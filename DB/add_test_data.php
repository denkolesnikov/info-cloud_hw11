<?php
require_once 'connect_db.php';

$table = 'publications';

try{

    $sqlRequests = [

        //-------------Articles-------------

        'INSERT INTO '.$table.' SET
	             title = "Voyager 1 just fired up some thrusters",
	             preview_text = "The only human-made object outside our solar system is still alive and kickin.",
	             text = "When Voyager 1\'s trajectory correction maneuver thrusters last fired, Ronald Reagan had just been elected president. Over 30 years ago, about a decade into the spacecraft\'s journey out to the edge of our solar system and beyond, the thrusters had officially served their purpose. The trajectory correction maneuver (TCM) thrusters sent out little puffs of power to correct the object\'s course, allowing Voyager 1 to explore Jupiter, Saturn, and several moons orbiting them. After the last course correction for Saturn on November 8, 1980, the TCMs went silent.",
	             author = "Rachel Feltman",
	             source = NULL;
	',
        'INSERT INTO '.$table.' SET
	             title = "Pluto is way cooler than it should be",
	             preview_text = "It has to do with a surprisingly hazy atmosphere.",
	             text = "It was a lazy, hazy, crazy day on Pluto when a spacecraft from Earth flew by at a blistering speed of 31,000 miles per hour two years ago.New Horizons took a bunch of snapshots, made some quick measurements of Pluto’s atmosphere, and sent them all back here, giving planetary scientists their first up-close look of the distant dwarf planet.",
	             author = "Mary Beth Griggs",
	             source = NULL;
	',
        'INSERT INTO '.$table.' SET
	             title = "One weird thing about eclipses",
	             preview_text = "Some drawings to explain the August eclipse\'s trajectory.",
	             text = "This summer, for the first time since 1918, a total solar eclipse will cut a path across the mainland United States. On August 21, everyone in North America will be able to watch the moon pass in front of the sun, blotting out some or all of its light (depending on where you live). People near Lincoln City, Oregon will see the total eclipse around 9:05am PDT. Then the path of totality slants eastward, finishing up in South Carolina at 2:43pm EDT.",
	             author = "Sarah Fecht",
	             source = NULL;
	',

        //-------------News-------------

        'INSERT INTO '.$table.' SET
	             title = "One in five ATMs now charge customers to withdraw cash",
	             preview_text = "Consumer watchdog Which? said it has \"significant concerns\" that plans by Link",
	             text = "One in five ATMs now charge customers to withdraw cash, consumer watchdog Which? says and is calling for an urgent review.It comes as thousands of machines are being shutdown.Which? said it has \"significant concerns\" has written to the Payment Systems Regulator (PSR),calling for it to conduct an urgent market review on the potential impact on consumers.",
	             author = NULL,
	             source = "BLOOMBERG NEWS";
	',
        'INSERT INTO '.$table.' SET
	             title = "Snow and ice set to cause chaos for commuters",
	             preview_text = "The worst snowfall in years will cause chaos for commuters on Monday",
	             text = "Almost a foot of powder is expected to be dumped across the country as temperatures plummet to -12 in the biggest whiteout since 2013. Snow and ice may reach as far as Devon and Cornwall and stretch up to the far north of Scotland and temperatures will struggle to rise above freezing, forecasters say.Approximately four inches of snow will fall within a three hour spell, known as a snow bomb, causing the most significant amount of chaos for years",
	             author = NULL,
	             source = "Agence France-Presse";
	',
        'INSERT INTO '.$table.' SET
	             title = "Facebook criticised over failure to stop counterfeit designer goods being sold",
	             preview_text = "Facebook has been criticised by Trading Standards over its failure to stop counterfeit designer goods being sold online.",
	             text = "It comes as counterfeit traders have been openly selling fake luxury brands on the social media\'s Marketplace site.An undercover investigation by BBC South East discovered that even after they had received fake goods ordered off the forum and reported it to Facebook the sellers were still continuing to operate.Mike Andrews, lead co-ordinator of the National Trading Standards eCrime Team, says Facebook needs to do more to tackle the problem.",
	             author = NULL,
	             source = "Reuters News Agency";
	',


    ];

    foreach ($sqlRequests as $sql){
        $pdoDB->exec($sql);
    };

}catch(PDOException $e){
	exit('cannot insert to table<br>'.$e->getMessage());
}